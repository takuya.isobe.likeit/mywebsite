package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Item;
import model.User;

/**
 * Servlet implementation class login
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//userDAOのインスタンス呼び出し
		UserDao userDao = new UserDao();
		//セッションスコープでUserクラスにあるuserInfoにログイン情報があるのかを判断
		//あった場合、ユーザー一覧へ遷移
		try {
			HttpSession session = request.getSession();
			User userINFO = (User)session.getAttribute("userInfo");
		if(userINFO !=null) {
			List<Item> itemList = userDao.findAllItem();
			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("itemList", itemList);
			// ユーザ一覧のjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
			} else {
		//セッション情報がない場合、login.jspへ遷移するフォワード文
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
		  }
		}catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");
		 	//index.jspで記入された数値を受け取る文
		 	String loginId = request.getParameter("loginId");
		 	//request.getParameterで入力欄の文字列を取得する
		 	//()内ダブルコーテーションで囲まれた文字列でname = loginIdと名前付けされた記入欄に記入された
		 	//文字を受け取り変数に入れている
			String password = request.getParameter("password");

			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			UserDao userDAO = new UserDao();
			//入力されたパスワードを暗号化
			try {
				String password00 = userDAO.password00(password);
				//userDAOのfindByLoginInfoを起動、()内の引数を渡す
				User user = userDAO.findByLoginInfo(loginId, password00);

			/** テーブルに該当のデータが見つからなかった場合 **/
			if (user == null) {
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "ログインに失敗しました。");

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);
				return;

			}

			/** テーブルに該当のデータが見つかった場合 **/
			// セッションにユーザの情報をセット
			HttpSession session = request.getSession();
			//(箱.数値の入った変数)箱をjspに渡しuser.loginIdでログインIDをjsp内に表記
			session.setAttribute("userInfo", user);

			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("Index");
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		}
}
