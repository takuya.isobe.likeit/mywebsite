package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Item;
import model.User;

/**
 * Servlet implementation class Buyresult
 */
@WebServlet("/Buyresult")
public class Buyresult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Buyresult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User userInfo = (User)  session.getAttribute("userInfo");
		Item finditem = (Item)  session.getAttribute("finditem");
		UserDao userDao = new UserDao();

		try {
			userDao.insertBuy(userInfo,finditem);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/AfterPurchase.jsp");
			dispatcher.forward(request, response);

			return;
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}















	}

}
