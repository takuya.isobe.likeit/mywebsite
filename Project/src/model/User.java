package model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;
	private String maker;
	private String detail;
	private int price;
	private String photo_name;
	private int status_id;
	private String status;

	//Search用コンストラクタ
	public User(int id,String loginId, String name) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;

	}
	// ログインセッションを保存するためのコンストラクタ
	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	// 全てのデータをセットするコンストラクタ

	//LoginIdを確認するためのコンストラクタ
	public User(String loginId) {
		this.loginId = loginId;
	}

	public User(int id, String maker, String name, String detail, int price, String photo_name, int status_id) {
		this.id = id;
		this.setMaker(maker);
		this.name = name;
		this.setDetail(detail);
		this.setPrice(price);
		this.setPhoto_name(photo_name);
		this.setStatus_id(status_id);
	}
	public User(int id, String loginId, String name, String password, String createDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.password = password;
		this.createDate = createDate;
	}
	public User(int id, String maker, String name, String detail, int price, String photo_name, String status) {
		this.id = id;
		this.setMaker(maker);
		this.name = name;
		this.setDetail(detail);
		this.setPrice(price);
		this.setPhoto_name(photo_name);
		this.setStatus(status);
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getPhoto_name() {
		return photo_name;
	}
	public void setPhoto_name(String photo_name) {
		this.photo_name = photo_name;
	}
	public int getStatus_id() {
		return status_id;
	}
	public void setStatus_id(int status_id) {
		this.status_id = status_id;
	}
	public int getuserId() {
		// TODO 自動生成されたメソッド・スタブ
		return 0;
	}
	public int getItem_id() {
		// TODO 自動生成されたメソッド・スタブ
		return 0;
	}

}
