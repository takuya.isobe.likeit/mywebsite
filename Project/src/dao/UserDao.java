package dao;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.Item;
import model.User;

/**
 * Servlet implementation class userDAO
 */
public class UserDao {

 public User findByLoginInfo(String loginId, String password) {
   Connection conn = null;
   try {
	   conn = DBM.getConnection();
	   //sqlのSELECT分を用意
	   String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
	// SELECTを実行し、結果表を取得
       PreparedStatement pStmt = conn.prepareStatement(sql);
       pStmt.setString(1, loginId);//左から1番目の?にLoginサーブレットからきた文字が入る
       pStmt.setString(2, password);
       ResultSet rs = pStmt.executeQuery();

       if (!rs.next()) {
           return null;
       }
       int id = rs.getInt("id");
       String loginIdData = rs.getString("login_id");
       //SQLから受け取った文字が()内に入っている、()内はカラム名を記入
       String nameData = rs.getString("name");
       return new User(id,loginIdData, nameData);
   } catch (SQLException e) {
       e.printStackTrace();
       return null;
   	} finally {
   	 // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            	}
        	}
   		}
 	}
 public List<User> findAll() {
	    Connection conn = null;
	    List<User> userList = new ArrayList<User>();

	    try {
	        // データベースへ接続
	        conn = DBM.getConnection();

	        // SELECT文を準備
	        String sql = "SELECT * FROM user";

	         // SELECTを実行し、結果表を取得
	        Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(sql);

	        // 結果表に格納されたレコードの内容を
	        // Userインスタンスに設定し、ArrayListインスタンスに追加
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String loginId = rs.getString("login_id");
	            String name = rs.getString("name");
	            String password = rs.getString("password");
	            String createDate = rs.getString("create_date");
	            User user = new User(id, loginId, name, password, createDate);

	            userList.add(user);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	    return userList;
	}
 public String password00(String password) throws NoSuchAlgorithmException {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		return result;

	}
 public User findLoginId(String loginId) {
	   Connection conn = null;
	   try {
		   conn  = DBM.getConnection();
		   //sqlのSELECT分を用意
		   String sql = "SELECT login_id FROM user WHERE login_id = ?";
		// SELECTを実行し、結果表を取得
	       PreparedStatement pStmt = conn.prepareStatement(sql);
	       pStmt.setString(1, loginId);
	       ResultSet rs = pStmt.executeQuery();

	       if (!rs.next()) {
	           return null;
	       }
	       String loginIdData = rs.getString("login_id");
	       return new User(loginIdData);
	   } catch (SQLException e) {
	       e.printStackTrace();
	       return null;
	   	} finally {
	   	 // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            	}
	        	}
	   		}
	 	}
 public void Register(String loginId, String name,String password)  {
	    Connection conn = null;
	    List<User> userList = new ArrayList<User>();
	    try {

	        // データベースへ接続
	        conn = DBM.getConnection();

	        // INSERT文を準備
	        String sql =  "INSERT INTO user(login_id,name,password,create_date) VALUES (?,?,?,now())";

	        // INSERTを実行
	        PreparedStatement stmt = conn.prepareStatement(sql);
	        stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, password);
	        stmt.executeUpdate();

	    } catch (SQLException e) {
	        e.printStackTrace();

	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();

	            }
	        }
	    }
	}
 public List<User> searchs(String name) {
	 Connection conn = null;
	 List<User> userList = new ArrayList<User>();
     try {
         // データベースへ接続
    	 conn  = DBM.getConnection();

         // SELECT文を準備
         String sql = "SELECT * FROM user WHERE name LIKE'%?%'";

         //SELECT * FROM user WHERE login_id = 'abc';
         //select*from user where  birth_date> '1995-01-01' and birth_date< '2000-12-30';
          // SELECTを実行し、結果表を取得

         // SELECTを実行し、結果表を取得
         Statement stmt = conn.createStatement();
         ResultSet rs = stmt.executeQuery(sql);

         // 結果表に格納されたレコードの内容を
         // Userインスタンスに設定し、ArrayListインスタンスに追加
         while (rs.next()) {
        	// String loginId = rs.getString("login_id");
             String Name = rs.getString("name");
             //Date birthDate = rs.getDate("birth_date");
             User user = new User(Name);


             userList.add(user);
         }
     } catch (SQLException e) {
         e.printStackTrace();
         return null;
     } finally {
         // データベース切断
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
                 return null;
             }
         }
     }
     return userList;
     }

//public List<User> search(String loginID,String name,String birthdayA,String birthdayB) {
//	 Connection conn = null;
//	 List<User> userList = new ArrayList<User>();
//	   try {
//		   conn  = DBM.getConnection();
//		   //sqlのSELECT分を用意
//		   String sql = "SELECT * FROM user WHERE id > 1";
//		   	if(!(loginID.equals(""))) {
//		   		sql += " and login_id ='"+ loginID +"'";
//		   	}
//		    if(!(name.equals("") )) {
//		   		sql += " and name LIKE '%" + name + "%'";
//		   	}
//		    if(!(birthdayA.equals("") )) {
//		   		sql += " and  birth_date BETWEEN'"+ birthdayA+ "'and'" +birthdayB+"'" ;
//		   	}
//
//		// SELECTを実行し、結果表を取得
//		   Statement stmt = conn.createStatement();
//	        ResultSet rs = stmt.executeQuery(sql);
//
//	       while (rs.next()) {
//               int id = rs.getInt("id");
//               String loginId = rs.getString("login_id");
//               String Name = rs.getString("name");
//               Date birthDate = rs.getDate("birth_date");
//               String password = rs.getString("password");
//               String createDate = rs.getString("create_date");
//               String updateDate = rs.getString("update_date");
//               User user = new User(id, loginId, Name, birthDate, password, createDate, updateDate);
//
//               userList.add(user);
//           }
//       } catch (SQLException e) {
//           e.printStackTrace();
//           return null;
//       } finally {
//           // データベース切断
//           if (conn != null) {
//               try {
//                   conn.close();
//               } catch (SQLException e) {
//                   e.printStackTrace();
//                   return null;
//               }
//           }
//       }
//       return userList;
//	}
public List<Item> findAllItem() {
    Connection conn = null;
    List<Item> itemList = new ArrayList<Item>();

    try {
        // データベースへ接続
        conn = DBM.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM item_deta INNER JOIN used_or_new ON item_deta.status_id = used_or_new .id;";

         // SELECTを実行し、結果表を取得
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
            int id = rs.getInt("id");
            String maker = rs.getString("maker");
            String name = rs.getString("name");
            String detail = rs.getString("detail");
            int price = rs.getInt("price");
            String photo_name = rs.getString("photo_name");
            String status = rs.getString("used_or_new.name");
            Item Item = new Item(id, maker, name,detail,price, photo_name,status);
            itemList.add(Item);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return itemList;
  }
public Item finditem(String id) {
	  Connection conn = null;
  try {
      // データベースへ接続
      conn = DBM.getConnection();
      // SELECT文を準備
      String sql = "SELECT *  FROM item_deta WHERE ID = ?";

     // 結果表に格納されたレコードの内容を
     // Userインスタンスに設定し、ArrayListインスタンスに追加
  	PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, id);
      ResultSet rs = pst.executeQuery();

      if (!rs.next()) {
          return null;
      }
      int Id = rs.getInt("id");
      String maker = rs.getString("maker");
      String name = rs.getString("name");
      String detail = rs.getString("detail");
      int price = rs.getInt("price");
      String photo_name = rs.getString("photo_name");
      int status_id = rs.getInt("status_id");
      return new Item(Id, maker, name,detail,price, photo_name,status_id);


} catch (SQLException e) {
  e.printStackTrace();
} finally {
  // データベース切断
  if (conn != null) {
      try {
          conn.close();
      } catch (SQLException e) {
          e.printStackTrace();
      }
    }
	}
	return null;
 }
public Item insertBuy(User userInfo,Item finditem) throws SQLException {
	 Connection conn = null;
	 PreparedStatement st = null;
	  try {
	      // データベースへ接続
	      conn = DBM.getConnection();
	      st = conn.prepareStatement(
	    	   "INSERT INTO buy_detail(user_id,item_id) VALUES(?,?)");
	      st.setInt(1, userInfo.getId());
		  st.setInt(2, finditem.getId());
		  st.executeUpdate();
	  } finally {
			if (conn != null) {
				conn.close();
			}
		}
	return null;
}

}
