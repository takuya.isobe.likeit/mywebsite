<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet" href="Materialize/css/login.css">
</head>
<body>

<c:if test="${errMsg != null}" >
  <div class="alert alert-danger" role="alert">
　　　${errMsg}
  </div>
</c:if>
<div class="login">

  <h2 class="login-header">ログイン</h2>

  <form class="login-container" action="login" method="post">
    <p><input type="text" placeholder="ID" name="loginId"></p>
    <p><input type="password" placeholder="Password" name="password"></p>
    <p><input type="submit" value="ログイン"></p>
  </form>

      <form class="Newuser" action="Newuser" method="get">
        <a href="Newuser">新規登録</a>
      </form>
</div>
</body>
</html>