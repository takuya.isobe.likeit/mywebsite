<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>検索・一覧ページ</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="Materialize/css/login.css">
</head>
<body>
<div>
   <div style=
         "padding: 20px;
          border: 1px solid #333333;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          width:900px;
          background-color: #28d;
          color: white;
          ">
    <ul class="nav navbar-nav navbar-right">
 		<div class="text-right">
            <li class="navbar-text">${userInfo.name} さん </li>
        　<u>
  			<li class="dropdown">
  			  <a href="Logout" class="navbar-link logout-link"style="color: green;">ログアウト</a>
  			   <a href="Allbuy" class="navbar-link logout-link"style="color: red;">購入履歴</a>
            </li>
          </u>
        </div>
        </ul>
    </div>
</div>

<div>
    <div style=
         "padding: 20px;
          margin-bottom: 1px;
          border: 1px solid #333333;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          background-color:#ebebeb;
          width:900px;">
      <h1><p class="text-center">
         検索・一覧
        </p></h1>

     <div class="form-loginID row">
        <label for="inputPassword"
               class="col-sm-0 col-form-label">
                <b>　車種名　　　　</b>
        </label>
            <div class="col-sm-9"
             align="center">

                <input type="text"
                       name="loginId"
                       class="form-control"
                       id="inputPassword"
                       style="width : 400px;">
            </div>
    </div>

    <p></p>

    <div class="form-loginID row">
        <label for="inputPassword"
               class="col-sm-0 col-form-label">
                <b>　メーカー　　　</b>
        </label>
          <div class="col-sm-9"
             align="center">
            <div class="col-sm-8"
             align="center">
                 <select id="inputState"
                         class="form-control">
                     <option selected>メーカー</option>
                     <option>...</option>
                </select>
              </div>
            </div>
    </div>
    <p></p>



    <p class="text-right">
            <button type="submit"
                    class="btn btn-primary" >　　検索　　
            </button>
    </p>

    <table border width="800"
       align="center">
        <tr align="center">
            <th bgcolor="#000055"
                style="color: white;">
                写真
            </th>
            <th bgcolor="#000055"
                style="color: white;">
                メーカー名
            </th>
            <th bgcolor="#000055"
                style="color: white;">
                バイク名
            </th>
            <th bgcolor="#000055"
                style="color: white;">
		状態
            </th>
            <th bgcolor="#000055"
                style="color: white;">

            </th>
        </tr>
	<c:forEach var="item" items="${itemList}">
         <tr align="center">
            <th>
                <input  type="image"
                        src="img/${item.photo_name}"
                        waidh="100px"
                        height="100px"
                        href="Bikeinfo?id=${item.id}">
            </th>
            <th>${item.maker}</th>
            <th>${item.name}</th>
            <th>${item.status}</th>
            <th>
                <a
                        type="submit"
                        class="btn btn-success"
                        href="Buy?id=${item.id}" >　　
                        購入　　
                </a>

                <a
                        type="submit"
                        class="btn btn-info"
                        href="Bikeinfo?id=${item.id}">　　
                        詳細　　
                </a>

            </th>
        </tr>
      </c:forEach>
    </table>



</div>
    </div>
</body>
</html>

